package main

import (
	"fmt"

	bigint "github.com/abbos-ron2/go/bigint/bigint"
)

func main() {

	a, err :=
		bigint.NewInt("-100")
	if err != nil {
		panic(err)
	}

	b, err := bigint.NewInt("100")
	if err != nil {
		panic(err)
	}

	fmt.Printf("Multiply: %v \n", bigint.Multiply(a, b).Value)
	fmt.Printf("Add: %s \n", bigint.Add(a, b).Value)
	fmt.Printf("Sub: %v \n", bigint.Sub(a, b).Value)
	a.Abs()
	fmt.Printf("Abs: %v \n", a.Value)
	a.Set("1")
	fmt.Printf("Set: %v \n", a.Value)
}
