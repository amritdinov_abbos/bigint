package bigint_test

import (
	"fmt"
	"testing"

	"github.com/abbos-ron2/go/bigint/bigint"
)

func NewInt(s string) (bigint.Bigint, error) {
	return bigint.NewInt(s)
}
func Add(a, b bigint.Bigint) bigint.Bigint {
	return bigint.Add(a, b)
}
func Sub(a, b bigint.Bigint) bigint.Bigint {
	return bigint.Sub(a, b)
}
func Multiply(a, b bigint.Bigint) bigint.Bigint {
	return bigint.Multiply(a, b)
}
func TestNewInt(t *testing.T) {
	//Positive
	str := "100"
	a, err := NewInt(str)
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	if a != (bigint.Bigint{Value: str}) {
		t.Errorf("NewInt FAILED: expected value %v but got %v", a, bigint.Bigint{Value: str})
	}

	str = "-100"
	a, err = NewInt(str)
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	if a != (bigint.Bigint{Value: str}) {
		t.Errorf("NewInt FAILED: expected value %v but got %v", a, bigint.Bigint{Value: str})
	}

	str = "-123456789123456789098765432123456"
	a, err = NewInt(str)
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	if a != (bigint.Bigint{Value: str}) {
		t.Errorf("NewInt FAILED: expected value %v but got %v", a, bigint.Bigint{Value: str})
	}

	str = "1312314131235678986564322345678767543212345678976543"
	a, err = NewInt(str)
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	if a != (bigint.Bigint{Value: str}) {
		t.Errorf("NewInt FAILED: expected value %v but got %v", a, bigint.Bigint{Value: str})
	}

	//Negative
	str = "131qqw6543"
	a, err = NewInt(str)
	if err == nil {
		t.Errorf("NewInt FAILED: expected error %v but got %v", bigint.ErrorBadInput, err)
	}

	str = "--31976543"
	a, err = NewInt(str)
	if err == nil {
		t.Errorf("NewInt FAILED: expected error %v but got %v", bigint.ErrorBadInput, err)
	}

	str = "131976543-"
	a, err = NewInt(str)
	if err == nil {
		t.Errorf("NewInt FAILED: expected error %v but got %v", bigint.ErrorBadInput, err)
	}

	str = "0131976543"
	a, err = NewInt(str)
	if err == nil {
		t.Errorf("NewInt FAILED: expected error %v but got %v", bigint.ErrorBadInput, err)
	}

	str = "+131976543"
	a, err = NewInt(str)
	if err == nil {
		t.Errorf("NewInt FAILED: expected error %v but got %v", bigint.ErrorBadInput, err)
	}
}
func TestAbs(t *testing.T) {
	//Positive
	str := "131976543"

	a, err := NewInt("-" + str)
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}

	a.Abs()
	if a.Value != (str) {
		t.Errorf("Abs FAILED: expected %v but got %v", str, a.Value)
	}

	a.Set(str)
	a.Abs()
	if a.Value != str {
		t.Errorf("Abs FAILED: expected %v but got %v", str, a.Value)
	}
}
func TestAdd(t *testing.T) {
	//Positive
	a, err := NewInt("100")
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	b, err := NewInt("100")
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}

	c := Add(a, b)
	if c.Value != "200" {
		t.Errorf("Add FAILED: expected value %v but got %v", bigint.Bigint{Value: "200"}, c)
	}

	a.Set("-100")
	b.Set("-100")
	c = Add(a, b)
	if c.Value != "-200" {
		t.Errorf("Add FAILED: expected value %v but got %v", bigint.Bigint{Value: "-200"}, c)
	}

	a.Set("-100")
	b.Set("100")
	c = Add(a, b)
	if c.Value != "0" {
		t.Errorf("Add FAILED: expected value %v but got %v", bigint.Bigint{Value: "0"}, c)
	}

	a.Set("100")
	b.Set("-100")
	c = Add(a, b)
	if c.Value != "0" {
		t.Errorf("Add FAILED: expected value %v but got %v", bigint.Bigint{Value: "0"}, c)
	}

	a.Set("100")
	b.Set("-99")
	c = Add(a, b)
	if c.Value != "1" {
		t.Errorf("Add FAILED: expected value %v but got %v", bigint.Bigint{Value: "1"}, c)
	}

	a.Set("100000")
	b.Set("-99")
	c = Add(a, b)
	if c.Value != "99901" {
		t.Errorf("Add FAILED: expected value %v but got %v", bigint.Bigint{Value: "99901"}, c)
	}
}
func TestSub(t *testing.T) {
	//Positive
	a, err := NewInt("100")
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	b, err := NewInt("100")
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}

	c := Sub(a, b)
	if c.Value != "0" {
		t.Errorf("Sub FAILED: expected value %v but got %v", bigint.Bigint{Value: "0"}, c)
	}

	a.Set("-100")
	b.Set("-100")
	c = Sub(a, b)
	if c.Value != "0" {
		t.Errorf("Sub FAILED: expected value %v but got %v", bigint.Bigint{Value: "0"}, c)
	}

	a.Set("-100")
	b.Set("100")
	c = Sub(a, b)
	if c.Value != "-200" {
		t.Errorf("Sub FAILED: expected value %v but got %v", bigint.Bigint{Value: "-200"}, c)
	}

	a.Set("100")
	b.Set("-100")
	c = Sub(a, b)
	if c.Value != "200" {
		t.Errorf("Sub FAILED: expected value %v but got %v", bigint.Bigint{Value: "200"}, c)
	}

	a.Set("100")
	b.Set("-99")
	c = Sub(a, b)
	if c.Value != "199" {
		t.Errorf("Sub FAILED: expected value %v but got %v", bigint.Bigint{Value: "199"}, c)
	}

	a.Set("100000")
	b.Set("-99")
	c = Sub(a, b)
	if c.Value != "100099" {
		t.Errorf("Sub FAILED: expected value %v but got %v", bigint.Bigint{Value: "100099"}, c)
	}
}
func TestMultiply(t *testing.T) {
	//Positive
	a, err := NewInt("100")
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	b, err := NewInt("100")
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}

	c := Multiply(a, b)
	fmt.Println(c)
	if c.Value != "10000" {
		t.Errorf("Multiply FAILED: expected value %v but got %v", bigint.Bigint{Value: "10000"}, c)
	}

	a.Set("-100")
	b.Set("100")
	c = Multiply(a, b)
	if c.Value != "-10000" {
		t.Errorf("Multiply FAILED: expected value %v but got %v", bigint.Bigint{Value: "-10000"}, c)
	}

	a.Set("-100")
	b.Set("-100")
	c = Multiply(a, b)
	if c.Value != "10000" {
		t.Errorf("Multiply FAILED: expected value %v but got %v", bigint.Bigint{Value: "10000"}, c)
	}

	a.Set("100")
	b.Set("-100")
	c = Multiply(a, b)
	if c.Value != "-10000" {
		t.Errorf("Multiply FAILED: expected value %v but got %v", bigint.Bigint{Value: "-10000"}, c)
	}
}
