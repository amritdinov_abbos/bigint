package bigint

import (
	"errors"
	"strconv"
	"strings"
)

type Bigint struct {
	Value string
}

var ErrorBadInput = errors.New("bad input, please write only number")

func NewInt(num string) (Bigint, error) {
	allowed := "1234567890"
	var err bool
	var str = num

	if strings.HasPrefix(str, "-") {
		str = strings.Replace(str, "-", "", 1)
	}
	if strings.HasPrefix(str, "0") {
		err = true
	}
	arr := strings.Split(str, "")
	for _, v := range arr {
		if !strings.Contains(allowed, v) {
			err = true
		}
	}

	if err {
		return Bigint{Value: num}, ErrorBadInput
	} else {
		return Bigint{Value: num}, nil
	}

}
func (x *Bigint) Set(num string) error {
	new, _ := NewInt(num)
	x.Value = new.Value
	return nil
}
func Add(a, b Bigint) Bigint {
	var IsNegativeResult bool
	n1, n2 := isPositive(a), isPositive(b)

	if n1 && !n2 {
		return Sub(a, makePositive(b))
	} else if !n1 && n2 {
		return Sub(b, makePositive(a))
	} else if !n1 && !n2 {
		IsNegativeResult = true
	}

	s1, s2 := makeStringSlice(a, b)
	s1, s2, length := makeSameLength(s1, s2, "start")

	var memory string
	var result []string

	//Iterating adding numbers from end
	for i := length - 1; i >= 0; i-- {
		var out []string

		numA, _ := strconv.ParseInt(s1[i], 10, 64)
		numB, _ := strconv.ParseInt(s2[i], 10, 64)
		res := numA + numB

		//Check for number in memory
		if memory != "" {
			m, _ := strconv.ParseInt(memory, 10, 64)
			res += m
			memory = ""
		}

		// Check number for two-digit, if true -> place first digit to memory
		if res >= 10 {
			out = strings.Split(strconv.Itoa(int(res)), "")
			memory = out[0]
			result = append([]string{out[1]}, result...)
		} else {
			r := strconv.Itoa(int(res))
			result = append([]string{r}, result...)
		}
	}

	//Check is there a number in memory after last iteration
	if memory != "" {
		result = append([]string{memory}, result...)
	}

	trimmed := trimString(strings.Join(result, ""))
	if IsNegativeResult {
		trimmed = "-" + trimmed
	}

	return Bigint{Value: trimmed}
}
func Sub(a, b Bigint) Bigint {
	n1, n2 := isPositive(a), isPositive(b)

	if !n1 && !n2 {
		return Add(makePositive(b), a)
	} else if n1 && !n2 {
		return Add(a, makePositive(b))
	} else if !n1 && n2 {
		return Add(a, makeNegative(b))
	}
	//If a < b, subtract a from b and return negative result
	if !isBigger(a, b) {
		answer := Sub(b, a)
		negtiveAnswer := strings.Split(answer.Value, "")
		negtiveAnswer = append([]string{"-"}, negtiveAnswer...)
		return Bigint{Value: strings.Join(negtiveAnswer, "")}
	}

	var memory int
	var result []string

	s1, s2 := makeStringSlice(a, b)
	s1, s2, length := makeSameLength(s1, s2, "start")

	//Iterating subtracting numbers from end
	for i := length - 1; i >= 0; i-- {
		var res int

		numA, _ := strconv.ParseInt(s1[i], 10, 64)
		numB, _ := strconv.ParseInt(s2[i], 10, 64)

		//Check for number in memory
		if memory != 0 {
			numA -= int64(memory)
			memory = 0
		}

		if numA >= numB {
			res = int(numA - numB)
		} else {
			res = 10 + int(numA-numB)
			memory = 1
		}

		result = append([]string{strconv.Itoa(res)}, result...)
	}

	trimmed := trimString(strings.Join(result, ""))
	return Bigint{Value: trimmed}
}
func (x *Bigint) Abs() {
	*x = makePositive(*x)
}
func Multiply(a, b Bigint) Bigint {
	var IsNegativeResult bool

	n1, n2 := isPositive(a), isPositive(b)

	//Checks should result be negative either positive
	if (n1 && n2) || (!n1 && !n2) {
		IsNegativeResult = false
	} else {
		IsNegativeResult = true
	}

	var smallerLength, biggerLength int
	var s1, s2 []string

	//Checks which argument is bigger, in order to multiply bigger to smaller
	if isBigger(a, b) {
		smallerLength = len(strings.Split(makePositive(b).Value, ""))
		biggerLength = len(strings.Split(makePositive(a).Value, ""))
		s1, s2 = makeStringSlice(makePositive(a), makePositive(b))
	} else {
		smallerLength = len(strings.Split(makePositive(a).Value, ""))
		biggerLength = len(strings.Split(makePositive(b).Value, ""))
		s1, s2 = makeStringSlice(makePositive(b), makePositive(a))
	}

	var memory string
	var numbers [][]string

	//Iterating multiplying numbers from end, and appending result to slice "numbers", in order to sum them
	for k := smallerLength - 1; k >= 0; k-- {
		var result []string

		for i := biggerLength - 1; i >= 0; i-- {
			var results []string
			numA, _ := strconv.ParseInt(s1[i], 10, 64)
			numB, _ := strconv.ParseInt(s2[k], 10, 64)
			res := numA * numB

			//Check for number in memory
			if memory != "" {
				m, _ := strconv.ParseInt(memory, 10, 64)
				res += m
				memory = ""
			}
			// Check number for two-digit, if true -> place first digit to memory
			if res >= 10 {
				results = strings.Split(strconv.Itoa(int(res)), "")
				memory = results[0]
				result = append([]string{results[1]}, result...)
			} else {
				r := strconv.Itoa(int(res))
				result = append([]string{r}, result...)
			}
		}
		//Check is there a number in memory after last iteration
		if memory != "" {
			result = append([]string{memory}, result...)
		}
		for j := 0; j < k; j++ {
			result = append([]string{"0"}, result...)
		}
		numbers = append(numbers, result)
	}
	//Append zeros to adding numbers
	for i := 0; i < len(numbers); i++ {
		_, v2, _ := makeSameLength(numbers[0], numbers[i], "end")
		numbers[i] = v2
	}
	var answer Bigint = Bigint{Value: "0"}
	//Adding numbers to each other
	for _, v := range numbers {
		answer = Add(answer, Bigint{Value: strings.Join(v, "")})
	}

	//Checks should result be positive either negative
	if IsNegativeResult {
		return makeNegative(answer)
	} else {
		return answer
	}
}

// func Mod(a, b Bigint) Bigint {
// }

// Additional functions
func isPositive(x Bigint) bool {
	if strings.HasPrefix(x.Value, "-") {
		return false
	} else {
		return true
	}
}
func makePositive(x Bigint) Bigint {
	if strings.HasPrefix(x.Value, "-") {
		x.Value = strings.Replace(x.Value, "-", "", 1)
	}
	return Bigint{Value: x.Value}
}
func makeNegative(x Bigint) Bigint {
	if !strings.HasPrefix(x.Value, "-") {
		s := strings.Split(x.Value, "")
		s = append([]string{"-"}, s...)
		res := strings.Join(s, "")
		return Bigint{Value: res}

	} else {
		return Bigint{Value: x.Value}

	}
}
func isBigger(a, b Bigint) bool {

	x1 := makePositive(Bigint{Value: a.Value})
	x2 := makePositive(Bigint{Value: b.Value})

	if len(x1.Value) > len(x2.Value) {
		return true
	} else if len(x1.Value) == len(x2.Value) {
		if x1.Value == x2.Value {
			return true
		}
		s1 := strings.Split(x1.Value, "")
		s2 := strings.Split(x2.Value, "")

		for i := 0; i < len(s1); i++ {
			num1, _ := strconv.ParseInt(s1[i], 10, 64)
			num2, _ := strconv.ParseInt(s2[i], 10, 64)
			if num1 > num2 {
				return true
			}
		}
		return false
	} else {
		return false
	}
}
func makeStringSlice(a, b Bigint) ([]string, []string) {
	x1 := strings.Split(makePositive(a).Value, "")
	x2 := strings.Split(makePositive(b).Value, "")

	return x1, x2
}
func trimString(x string) string {
	for strings.HasPrefix(x, "0") && len(x) > 1 {
		x = strings.Replace(x, "0", "", 1)
	}
	return x
}
func makeSameLength(a, b []string, mode string) ([]string, []string, int) {
	var bigger bool
	var change int
	var length int

	var x1 = a
	var x2 = b

	if len(a) > len(b) {
		length = len(a)
		bigger = true
		change = len(a) - len(b)
	} else {
		bigger = false
		length = len(b)
		change = len(b) - len(a)
	}

	if mode == "start" {
		if bigger {
			for i := 0; i < change; i++ {
				x2 = append([]string{"0"}, x2...)
			}
		} else {
			for i := 0; i < change; i++ {
				x1 = append([]string{"0"}, x1...)
			}
		}
	} else if mode == "end" {
		if bigger {
			for i := 0; i < change; i++ {
				x2 = append(x2, "0")
			}
		} else {
			for i := 0; i < change; i++ {
				x1 = append(x1, "0")
			}
		}
	}
	return x1, x2, length
}
